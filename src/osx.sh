#!/usr/bin/env sh
##~---------------------------------------------------------------------------##
##                                                                            ##
##    _____ __ __ __ _____ __       _ _ _ __ _____ _____ _____ ____  _____    ##
##   |  _  |  |  |  |   __|  |     | | | |  |__   |  _  | __  |    \|   __|   ##
##   |   __|  |-   -|   __|  |__   | | | |  |   __|     |    -|  |  |__   |   ##
##   |__|  |__|__|__|_____|_____|  |_____|__|_____|__|__|__|__|____/|_____|   ##
##                           http://pixelwizards.io                           ##
##                                                                            ##
##  File      : osx.sh                                                        ##
##  Project   : pw_shellscript_utils                                          ##
##  Date      : Jul 12, 2018                                                  ##
##  License   : GPLv3                                                         ##
##  Author    : stdmatt <stdmatt@pixelwizards.io>                             ##
##  Copyright : pixelwizards - 2018                                           ##
##                                                                            ##
##  Description :                                                             ##
##    Things that only works on OSX.                                          ##
##                                                                            ##
##    All functions started with the prefix: "_pw_impl_" are implementaion    ##
##    of some public API defined in main.sh. Users aren't expected to         ##
##    directly call those functions, but the public api counterparts.         ##
##                                                                            ##
##    All other stuff that are specific to OSX must be prefixed               ##
##    with: "pw_osx_" to let clear that this only works on this OS.           ##
##---------------------------------------------------------------------------~##


##----------------------------------------------------------------------------##
## Public API Implementation                                                  ##
##----------------------------------------------------------------------------##
###-----------------------------------------------------------------------------
_pw_impl_get_user_home()
{
    local TARGET_USER="$1";
    test -z "$TARGET_USER" && TARGET_USER=$(whoami); ## No user given, default to current one.

    ## @hack(stdmatt): Probably there's a better way to do that,
    ##    but I'm starting with OSX internals right now.
    local HOMEDIR=$(dscl . -read /Users/$TARGET_USER NFSHomeDirectory 2> /dev/null | cut -d' ' -f2)
    echo "$HOMEDIR";
}

###-----------------------------------------------------------------------------
_pw_impl_os_get_simple_name()
{
    echo "$(PW_OS_OSX)";
}

###-----------------------------------------------------------------------------
_pw_imp_get_file_mode()
{
    stat -f "%p" "$1";
}

###-----------------------------------------------------------------------------
_pw_imp_get_file_owner()
{
    stat -f "%Su" "$1";
}

###-----------------------------------------------------------------------------
_pw_imp_get_file_group()
{
    stat -f "%Sg" "$1";
}